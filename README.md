---
title: MakeSense
---

# Purpose

MakeSense strives to be a smart email client, providing insights into your
communication habits so you can make informed decisions and plan for the
future.

Some example insights might include:

- Your most frequently contacted recipient is "Jane Doe" with an average of
  5 emails per week.
- The person you contact most is Mr. Boss
- You never seem to reply to Ms. Spamington
- You receive 87% of your emails bewteen 8AM and 4PM
- 90% of your contacts reply within 3 days.

The application is designed to be plugin-friendly. See the `README`
in the `simple_print` plugin for details on how to create a plugin.

# Disclaimer

This project is very much in development; contributions are welcome!

# Design

## Overview

```
makesense
  +--- makesense-server (Flask & SQLAlchemy)
    +-- (built-in plugins)
  +--- makesense-electron (Front-end container)
    +--- makesense-lib (MakeSense TypeScript library for types, functions, etc.)
    +--- makesense-vue (front-end UI; links makesense-lib)
```

## Details

### Front-End

MakeSense aims to be **cross-platform**, which means (in this day an age) the front-
end is built with electron. The framework associated with the electron app is
Vue.js built with TypeScript.

### Back-End

I don't think there is a good cross-platform back-end, so I had to go with one with
a fairly high adoption rate (I'll attempt a turnkey install solution in the end).

So the back-end uses Flask as the API framework, and SQLAlchemy as the storage
mechanism.

I chose Python because it's used for analytics and data science, so it made sense to just
incorporate it as such.

Note that **there is no authentication**, since this project aims to be an in-house 
solution. So, basically, if someone unauthorized has access to this application, there
are a lot more things to worry about. Authentication may be required in the future, but
at this point, I don't see a need.

### Application Flow

1. The back-end starts. There are hooks that are called thorught the course of the
   application. These hooks are associated with a `PluginFinder`, which
   
   a. looks for installed (and enabled) plugins on the system
 
   b. imports the plugin as a Python Module, and

   c. Executes the hook of the plugin.

2. The front-end starts. The front-end will call the back-end `/plugins` endpoint, which
   contains information on the plugins installed. These will be loaded as HTML code along
   with necessary metadata (e.g. tab location).

3. Mail fetching is handled by the back-end. An IMAP engine fetches mail from the server
   and stores it in the database given by SQLAlchemy. Associated hooks are called during
   the fetching and storage of the message.

# Developer Notes

## Email Data Generation -- Real and Mock

For initial testing, I created a command line utility for creating an Email account and
downloading the data.

```bash
cd makesense-server
python -m makesense.cli.account
```

```
==== Usage ====

Usage: account.py [OPTIONS] COMMAND [ARGS]...

Options:
  --server-mode TEXT
  --db-url TEXT
  --help              Show this message and exit.

Commands:
  add-server
  download

Usage: account.py add-server [OPTIONS]

Options:
  -o, --incoming-host TEXT
  -t, --incoming-server-type [imap]
                                  Type of incoming server
  -p, --incoming-port INTEGER
  -u, --incoming-username TEXT
  --incoming-password TEXT
  -O, --outgoing-host TEXT
  -T, --outgoing-server-type [smtp]
                                  Type of outgoing server
  -P, --outgoing-port INTEGER
  -U, --outgoing-username TEXT
  --outgoing-password TEXT
  -M, --master, --with--master-password / -N, --no-master, --without-master-password
  --help                          Show this message and exit.

Usage: account.py download [OPTIONS]

Options:
  --servers LIST
  --mock
  --help          Show this message and exit.

```

For testing you can download your real email or, if you don't want to do that,
messages can be mocked with `python-faker` using the `--mock` flag, e.g.

```bash
python -m makesense.cli.account download --mock
```

## Starting The Back-End

The back-end code resides in `makesense-backend`

```bash
cd makesense-server
# Make sure poetry is installed! https://python-poetry.org/
poetry install
poetry run ./manage.py runserver
```

## Starting the Front-End

> ### Aside
> The plan is to have the Vue application built for loading into the electron app.
> For now developent is done as a web app.

```bash
cd makesense-electron/makesense-vue
# Make sure yarn (a node.js/npm application) is installed!
yarn install
# You might need to do the following (do this only if
# you run into a "not found" type error)
cd ../makesense-lib
yarn link .
cd ../makesense-vue
yarn link makesense-lib
# Now you're ready to start the server!
yarn dev
```

The front-end should be available at https://localhost:8080

### Odd Vue Build Errors

If this happens, I found the following work around.

In one of the config files (I forget which one--I think `makesense-vue/tsconfig.json`),
comment out one of the lines (I forget exactly which one--I think `"webpack-env"` under
`"types"`.).

Run the `yarn` command again. This will produce more errors, but that's okay. 

Hit `ctrl+c` and uncomment the line.

Running the `yarn` command again will produce a successful build.

## Feature TODO

- Allow PluginFinder to register hooks for use on the front-end portion
  of the plugins.
- Add a hooks for scheduling cron tasks that are
  - action based (e.g. when a user clicks on a button)
  - time-based (e.g. every five minutes)
- Ability to connect to IMAP and send messages
  - Add back-end REST rooks to send message on POST request
  - Add UI functionality to compose a message
- Include other account types, like Open-Xchange
- Clean up UI
  - Make it more material-like.
    - Account menu along the left side
    - "Plus sign" to add account
- Integrate with electron
- Create turnkey solution for backend installation & startup to make it
  more cross-platform
- Front-end integration tests
- Test on
  - Windows
  - Mac