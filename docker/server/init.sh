#!/bin/bash

# Set common environment variables
export GETPOETRY="https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py"
export APPHOME="/app/server"
export PYTHON_TARGET="python3"
# Comment to enable apk caching.
# export NOCACHE="--no-cache"

# Helpful functions
function addToPath() {
    [[ ":$PATH:" != *":$1:"* ]] && PATH="$1:${PATH}"
}

######################################
# This is where the magic happens!
#

apk add git curl gcc ${PYTHON_TARGET} ${PYTHON_TARGET}-dev libc-dev openssl-dev musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev

# Install curl
alias python=${PYTHON_TARGET}

# Install poetry
curl -sSL ${GETPOETRY} | python
addToPath ${HOME}/.poetry/bin
source $HOME/.poetry/env

# Use poetry to install app requirements
cd ${APPHOME}
poetry install

# Run Migrations
poetry run ./repo/manage.py version_control ${DATABASE_URL}
poetry run ./repo/manage.py upgrade --url=${DATABASE_URL}
# /bin/sh